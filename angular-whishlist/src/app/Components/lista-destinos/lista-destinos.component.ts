import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DestinosVisita } from 'src/app/Models/destinos-visita.model';
import { DestinosApiClient } from 'src/app/Models/destinos-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.module';



@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosApiClient]
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinosVisita>;
  updates: string[];
  all:DestinosVisita[] = [];
  
  
  constructor(private destinosApiClient: DestinosApiClient, private store: Store<AppState>) { 
    this.onItemAdded = new EventEmitter();
    this.updates = [];
  }

  ngOnInit() {
    this.store.select(state => state.destinos.favorito)
    .subscribe(data => {
      const f = data;
      if (f != null) {
        this.updates.push('Se elegio: ' + f.nombre);
      }
    });
  }

  agregado(d: DestinosVisita) {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }

  elegido(e: DestinosVisita) {
    this.destinosApiClient.elegir(e);
  }
}

