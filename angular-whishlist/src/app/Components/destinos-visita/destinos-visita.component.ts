import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.module';
import { VoteDownAction, VoteUpAction } from 'src/app/Models/destinos-visita-state.model';
import { DestinosVisita } from 'src/app/Models/destinos-visita.model';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-destinos-visita',
  templateUrl: './destinos-visita.component.html',
  styleUrls: ['./destinos-visita.component.css'],
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgrounColor: 'WhiteSmoke'
      })), 
      transition('estadoNoFavorito => estadoFavorito', [
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('1s')
    ]),
  ])
]
})

export class DestinosVisitaComponent implements OnInit {
  @Input() destino!: DestinosVisita;
  @Input('idx') position!: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() onClicked!: EventEmitter<DestinosVisita>;

  constructor(private store: Store<AppState>) {
    this.onClicked = new EventEmitter();
  }
  
  ngOnInit() {
  }

  ir() {
    this.onClicked.emit(this.destino);
    return false;
  }

  voteUp() {
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }

  voteDown() {
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }

}
 