import {
    reducerDestinosVisita,
    DestinosVisitaState,
    initializeDestinosVisitaState,
    InitMyDataAction,
    NuevoDestinoAction
  } from '../Models/destinos-visita-state.model';
  import { DestinosVisita } from '../Models/destinos-visita.model';
  
  describe('reducerDestinosViajes', () => {
    it('should reduce init data', () => {
    //setup
      const prevState: DestinosVisitaState | any = initializeDestinosVisitaState();
      const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2']);
    // action
      const newState: DestinosVisitaState = reducerDestinosVisita(prevState, action);
    //assert
      expect(newState.items.length).toEqual(2);
      expect(newState.items[0].nombre).toEqual('destino 1');
    });
  
    it('should reduce new item added', () => {
      const prevState: DestinosVisitaState | any = initializeDestinosVisitaState();
      const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinosVisita('Merida', 'url'));
      const newState: DestinosVisitaState = reducerDestinosVisita(prevState, action);
      expect(newState.items.length).toEqual(1);
      expect(newState.items[0].nombre).toEqual('Merida');
    });
  });