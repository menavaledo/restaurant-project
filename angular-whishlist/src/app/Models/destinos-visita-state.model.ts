import { Injectable } from '@angular/core'
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DestinosVisita } from '../Models/destinos-visita.model';
import { HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';

// ESTADO
export interface DestinosVisitaState {
    items: DestinosVisita[];
    loading: boolean;
    favorito: DestinosVisita;
}

export function initializeDestinosVisitaState() {
    return {
        items: [],
        loading: false,
        favorito: null
    };
}

// ACCIONES
export enum DestinosVisitaActionTypes {
    NUEVO_DESTINO = '[Destinos Visita] Nuevo',
    ELEGIDO_FAVORITO = '[Destinos Visita] Favorito',
    VOTE_UP = '[Destinos Visita] Vote Up',
    VOTE_DOWN = '[Destinos Visita] Vote Down',
    INIT_MY_DATA = '[Destinos Visita] Init My Data',
}

export class NuevoDestinoAction implements Action {
    type = DestinosVisitaActionTypes.NUEVO_DESTINO;
    constructor(public destino: DestinosVisita) {}
}

export class ElegidoFavoritoAction implements Action {
    type = DestinosVisitaActionTypes.ELEGIDO_FAVORITO;
    constructor(public destino: DestinosVisita) {}
}

export class VoteUpAction implements Action {
    type = DestinosVisitaActionTypes.VOTE_UP;
    constructor(public destino: DestinosVisita) {}
}

export class VoteDownAction implements Action {
    type = DestinosVisitaActionTypes.VOTE_DOWN;
    constructor(public destino: DestinosVisita) {}
}

export class InitMyDataAction implements Action {
    type = DestinosVisitaActionTypes.INIT_MY_DATA;
    constructor(public destinos: string[]) {}
}


export type DestinosVisitaActions = NuevoDestinoAction | ElegidoFavoritoAction 
| VoteUpAction | VoteDownAction | InitMyDataAction;

//REDUCERS 
export function reducerDestinosVisita(
    state: DestinosVisitaState,
    action: DestinosVisitaActions
): DestinosVisitaState {
    switch (action.type) {
        case DestinosVisitaActionTypes.INIT_MY_DATA: {
            const destinos: string[] = (action as InitMyDataAction).destinos;
            return {
                ...state,
                items: destinos.map((d) => new DestinosVisita(d, ''))
            }
        }
        case DestinosVisitaActionTypes.NUEVO_DESTINO: {
            return {
                ...state, 
                items: [...state.items, (action as NuevoDestinoAction).destino]
            };
        }
        case DestinosVisitaActionTypes.ELEGIDO_FAVORITO: {
            state.items.forEach(x => x.setSelected(false));
            const fav: DestinosVisita = (action as ElegidoFavoritoAction).destino;
            fav.setSelected(true);
            return {
                ...state,
                favorito: fav
            };
        }
        case DestinosVisitaActionTypes.VOTE_UP: {
            const d: DestinosVisita = (action as VoteUpAction).destino;
            d.voteUp();
            return { ...state };
        }
        case DestinosVisitaActionTypes.VOTE_DOWN: {
            const d: DestinosVisita = (action as VoteDownAction).destino;
            d.voteDown();
            return { ...state };
        }
    }
    return state;
}

// EFFECTS
@Injectable()
export class DestinosVisitaEffects {
    @Effect()
    nuevoAgregado$: Observable<Action> = this.actions$.pipe(
        ofType(DestinosVisitaActionTypes.NUEVO_DESTINO),
        map((action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
    );
    constructor(private actions$: Actions) {}
}